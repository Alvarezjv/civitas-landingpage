

/* get DOM elements */

const inputUsuario = document.getElementById("User");
const inputEmail = document.getElementById("Email");
const inputNascimento = document.getElementById("Nascimento");
const inputSenha = document.getElementById("Password")
const createBtn = document.getElementById("Botao");
const errSpan = document.querySelector(".error");


const postChores = async (Usuario, Email, Nascimento, Senha) => {
    console.log(Usuario);
    try{
        const response = await fetch("http://localhost:3000/usuarios",{
                    method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                Usuario: `${Usuario}`,
                Email: `${Email}`,
                Nascimento: `${Nascimento}`,
                Senha: `${Senha}`,
            }),
        });
        const content = await response.json();
        return content;
    }catch (error){
            console.log(error);
        }

};


/ disparar evento para criar nova Chore /
createBtn.addEventListener('click', async (event) => {
    event.preventDefault();
    const Usuario = inputUsuario.value;
    const Email = inputEmail.value;
    const Nascimento = inputNascimento.value;
    const Senha = inputSenha.value;

  / verificar se os campos estão preenchidos */
  if (Usuario === "" || Email === "" || Nascimento === "" || Senha === "") {
    errSpan.style.display = "flex"
  } else {
    errSpan.style.display = "none";
    await postChores(Usuario, Email, Nascimento, Senha);
  }
});