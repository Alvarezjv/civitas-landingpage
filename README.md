<strong>1) Conteúdo do projeto</strong>

JavaScript - DOM <br>
HTTP protocol <br>
CRUD - HTTP methods <br>
Fetch API

<br>

<strong>2) tecnologias e ferramentas</strong>

JavaScript <br>
Fetch API <br>
JSON Server <br>

<br>

<strong>3) rodando o projeto na máquina</strong>

<strong>Clone esse repositório --></strong>

<strong>.</strong> https://gitlab.com/Alvarezjv/civitas-landingpage.git

<strong>Entre na pasta do projeto --></strong>

<strong>.</strong> civitas-landingpage

<strong>Abra o terminal (pode ser pelo VSCode) e execute o comando para instalar as dependências --></strong>

<strong>.</strong> npm install

<strong>Execute o comando para iniciar a API --></strong>

<strong>.</strong> npx json-server --watch db.json

<strong>Abra o arquivo index.html para iniciar a aplicação</strong>

Para parar de servir a API, basta executar o comando CTRL + C no terminal.

 <br>

 <strong>Created by Alvarez